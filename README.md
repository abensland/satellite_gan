# GANs Project: Number and Satellite Image Generators

## Team Members
- Alexander Bensland
- Rico Benning
- Timon Plenge

## Project Overview
This project explores different implementations of Generative Adversarial Networks (GANs) using PyTorch. We focused primarily on Deep Convolutional GANs (DCGANs) to generate various types of images. Our implementations include:

1. A black and white number image generator.
2. A colored number generator.
3. A higher-definition (32x32x3) colored satellite image generator.

We optimized our implementations for CUDA and Metal to leverage GPU acceleration for faster training. Additionally, we implemented a save and load feature to store the trained network models.

We also made the code compatible with Google Colab and Google Drive, allowing you to save the models directly to your Google Drive. This ensures that your work is preserved even if you are disconnected from Google Colab.



## Repository Structure
```
├── Images
│   ├── Some generated Images of our project
├── Saves
│   ├── names of saved models
├── notebooks
│   ├── color_gen.ipynb
│   ├── number_gan.ipynb
│   └── satellite_gan_fail.ipynb
│   └── satellite_gan_metal.ipynb
│   └── satellite_gan_save.ipynb
├── DataTesting.ipynb
└── README.md
```



### Project Overview
Our project explores various implementations of Generative Adversarial Networks (GANs) using PyTorch. We initially aimed to generate high-definition satellite images but encountered several challenges. Here's a brief overview of our different approaches and their outcomes:

### Failed Satellite GAN
1. `satellite_gan_fail.ipynb`:
   - This was our first implementation, but it failed to converge to the desired loss. We suspected that the failure might be due to insufficient training time or an overly simplistic architecture.

### Improved Satellite GAN with Longer Training
1. `satellite_gan_save.ipynb`:
   - To address the issues with the initial implementation, we created a model that could be saved and trained for longer periods using Google Colab. This allowed us to extend the training duration, but the results still did not meet our expectations.

### Complex Satellite GAN Utilizing Metal
1. `satellite_gan_metal.ipynb`:
   - We also tried developing a more complex model utilizing Metal for better performance. Despite these efforts, the GAN still did not produce the desired results.

### Black and White Number Generator
1. `number_gan.ipynb`:
   - After the satellite GAN did not produce the correct images even with sufficient training, we decided to switch to a simpler dataset. We implemented a black and white number generator to ensure we could create a working GAN with more straightforward data.

### Colored Number Generator
1. `color_gen.ipynb`:
   - We then attempted to generate colored images by first creating a black and white number generator and then adding color. However, this approach did not yield convincing results, and the coloring did not solve our initial issues with image generation.



So despite not achieving our start goal, we learned alot about GAN training and the problems with generating high-quality colored images. 


### Training on GPU
The code automatically detects and uses the GPU if cuda or metal is available.

### Saving the Model
The trained models are saved in the `Saves` directory. You can load these models for inference or further training.


## Future Work
- Fix satellite Gan by changing the loss function and maybe the architeture of the Generator.
- Experiment with different GAN architectures and loss functions.
- Explore conditional GANs for more controlled image generation.